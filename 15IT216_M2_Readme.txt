DroidDeepLearner: Identifying Android Malware Using Deep Learning

Project by: 
- M Amith Kini [15IT216]
- Swastik Udupa [15IT148]

Steps:
1) Use androguard 3.1 to extract the Permissions from the android file. Automate this to extract the permission from all the apk files.
2) Create a vector against each of the APK and mark 1 or 0, depending on if the permission is present in the given APK file or not.
3) Use the methods of Androguard 3.1 to extract the API calls from one of the APK’s and then automate the process to extract the calls from all the APK’s that are present.
4) Use feature selection to filter permissions to 100 from a total of over 500 features.
5) Create a vector against each of the APK’s and mark 1 or 0, depending on the presence of API calls or not. The API calls whose presence have to be checked are taken from the paper ‘DroidAPIMiner: Mining API-Level Features for Robust Malware Detection in Android’ where the top 20 API calls found in malwares are listed.
6) We use the two vectors i.e Vector created from the Android permissions and the Vector created from API calls as an input for the neural network.
7) We train a deep belief network with the processed data to predict the type of input.
8) We compare the following results of accuracy with other models.

Running the code

1) Open the folder 15IT216_M2/src/ .
2) Create a virtual environment and activate:
	a) Open a terminal at the folder location and execute: python3 -m venv ve
	b) Source it: source ve/bin/activate
3) Download androguard: $ git clone https://github.com/androguard/androguard.git
4) cd into androguard folder: cd androguard
5) Install the setup: pip install .
6) Go to code folder: cd ../code
7) Install the remaining requirements: pip install -r requirements.txt
8) To get the count of each features, use the bash script: $ bash script.sh feature_count.py path/to/database. After this 2 pickle files will be created in pickle/ folder. Rename both files to mal_<filename>.pkl or ben_<filename>.pkl according to the database selected.
9) For the feature selection, run: $ python feature_sel.py
10) To generate the vector, run $ bash script.sh vector.py path/to/database malware or $ bash script.sh vector.py path/to/database benign  : depending upon the database
11) To train the DBN, run: $ python dbn.py . For each model with different number of hidden layers, the model is saved in models/ folder.
12) To train and test the SVN, we use $ python svn.py

