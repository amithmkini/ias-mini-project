import os
import gc
import sys
import numpy as np
import pandas as pd
import sys
import pickle
# from api_calls import *

filename = sys.argv[1]
type_of = sys.argv[2]

if type_of == 'malware':
    class_ = 'M'
else:
    class_ = 'B'

CORPUS_FOLDER = './'
ANDROGUARD_FOLDER = '../../androguard'
PERM_CSV_FILE = 'perm_data.csv'
API_CSV_FILE = 'api_data.csv'
CONNECTOR = '->'
PERMISSION_HEAD = 'pickle/fil_perm.pkl'
API_HEAD = 'pickle/fil_api.pkl'


sys.path.append(ANDROGUARD_FOLDER)
import androlyze as anz

permission_list = pickle.load(open(PERMISSION_HEAD, "rb"))
api_list = pickle.load(open(API_HEAD, "rb"))

permission_list += ['class']
api_list += ['class']

if not os.path.isfile(PERM_CSV_FILE):
    # Since the file is not there, we need to create a blank file
    df = pd.DataFrame(index=[], columns=permission_list)
    df = df.fillna(0)
    df.to_csv(PERM_CSV_FILE)

if not os.path.isfile(API_CSV_FILE):
    # Since the file is not there, we need to create a blank file
    df = pd.DataFrame(index=[], columns=api_list)
    df = df.fillna(0)
    df.to_csv(API_CSV_FILE)


try:
    APK_FILE = filename
    print("Processing",os.path.join(CORPUS_FOLDER, APK_FILE))
    filename = filename.split('/')[-1]
    apk, dvmf, da = anz.AnalyzeAPK(os.path.join(CORPUS_FOLDER, APK_FILE))
    permissions = apk.get_permissions()

    perm_cols = [1 if x in permissions else 0 for x in permission_list[:-1]]
    perm_cols += [class_]
    perm_cols = [perm_cols]
    perm_cols = np.array(perm_cols)

    ext = list(da.get_external_classes())
    mthd_list = list()
    for cls_ans in ext:
        vm_cls = cls_ans.get_vm_class()
        # Skip the ; in the end
        name = vm_cls.get_name()[:-1]
        methods = vm_cls.get_methods()
        for method in methods:
            mthd_name = method.get_name()
            mthd_list.append(name + CONNECTOR + mthd_name)

    final_list = [1 if x in mthd_list else 0 for x in api_list[:-1]]
    final_list += [class_]
    api_cols = [final_list]
    api_cols = np.array(api_cols)

    df = pd.DataFrame(perm_cols, index=[filename], columns=permission_list)
    with open(PERM_CSV_FILE, 'a') as f:
        df.to_csv(f, header=False)

    df2 = pd.DataFrame(api_cols, index=[filename], columns=api_list)
    with open(API_CSV_FILE, 'a') as f:
        df2.to_csv(f, header=False)

except Exception as e:
    print(e)
    print("Skipping {}".format(filename))
