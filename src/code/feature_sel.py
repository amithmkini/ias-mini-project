import os
import sys
import pickle
from collections import OrderedDict
from api_calls import MAPPINGS

mal_api = pickle.load(open("pickle/mal_api_list_dict.pkl", "rb"))
mal_perm = pickle.load(open("pickle/mal_perm_list_dict.pkl", "rb"))
ben_api = pickle.load(open("pickle/ben_api_list_dict.pkl", "rb"))
ben_perm = pickle.load(open("pickle/ben_perm_list_dict.pkl", "rb"))

count_mal = 5560
count_ben = 56

# a = {'perm1':2, 'perm2':3, 'perm3':4, 'perm4':10}
# b = {'perm1':1, 'perm2':5, 'perm3':2}

diff_api = OrderedDict()
diff_perm = OrderedDict()

# for keys in mal_api:
#     try:
#         diff_api[keys] = (mal_api[keys]/count_mal) - (ben_api[keys]/count_ben)
#     except:
#         diff_api[keys] = (mal_api[keys]/count_mal)
# diff_api = sorted(diff_api, key=diff_api.get, reverse=True)[:100]

diff_api = MAPPINGS



for keys in mal_perm:
    try:
        diff_perm[keys] = (mal_perm[keys]/count_mal) - (ben_perm[keys]/count_ben)
    except:
        diff_perm[keys] = (mal_perm[keys]/count_mal)

diff_perm = sorted(diff_perm, key=diff_perm.get, reverse=True)[:100]

pickle.dump(diff_api, open("pickle/fil_api.pkl", "wb"))
pickle.dump(diff_perm, open("pickle/fil_perm.pkl", "wb"))
