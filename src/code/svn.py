import pandas as pd
import numpy as np

df = pd.read_csv('perm_data.csv')
df = df.replace('M', 1)
df = df.replace('B', 0)
X1 = df.as_matrix()[:, 1:-1]
final_y = df.as_matrix()[:,(-1)]



df = pd.read_csv('api_data.csv')
df = df.replace('M', 1)
df = df.replace('B', 0)
X2 = df.as_matrix()[:, 1:-1]

final_X =np.hstack((X1, X2))


from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
X_train, X_test, Y_train, Y_test = train_test_split(final_X, final_y, test_size=0.2, random_state=0, shuffle=True)


from sklearn.svm import OneClassSVM
clf_oneclass = OneClassSVM(kernel='rbf')

print("Training SVN...")
clf_oneclass.fit(X_train, Y_train)


y_pred = clf_oneclass.predict(X_test)
for num in range(len(y_pred)):
    if(y_pred[num]==-1):
        y_pred[num] = 0
        
correct = 0
total = 0

for i in range(len(y_pred)):
    if y_pred[i]==1:
        total+=1
    if(y_pred[i]==Y_test[i] and y_pred[i]==1):
        correct += 1
print("Precision: ",correct/total)
