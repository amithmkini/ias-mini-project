import os
import gc
import sys
import numpy as np
import pandas as pd
import sys
import pickle
# from api_calls import *
from collections import OrderedDict


filename = sys.argv[1]

CORPUS_FOLDER = './'
ANDROGUARD_FOLDER = '../../androguard'
PERM_CSV_FILE = 'perm_data.csv'
API_CSV_FILE = 'api_data.csv'
CONNECTOR = '->'
PERMISSION_LIST = 'pickle/perm_list_dict.pkl'
API_LIST = 'pickle/api_list_dict.pkl'

# Import Androguard
sys.path.append(ANDROGUARD_FOLDER)
import androlyze as anz

try:
    permission_list = pickle.load(open(PERMISSION_LIST, "rb"))
except Exception as e:
    permission_list = OrderedDict()

try:
    api_list = pickle.load(open(API_LIST, "rb"))
except Exception as e:
    api_list = OrderedDict()

try:
    APK_FILE = filename
    print("Processing",os.path.join(CORPUS_FOLDER, APK_FILE))

    apk, dvmf, da = anz.AnalyzeAPK(os.path.join(CORPUS_FOLDER, APK_FILE))
    permissions = apk.get_permissions()

    for perm in permissions:
        if perm in permission_list.keys():
            permission_list[perm] += 1
        else:
            permission_list[perm] = 1

    pickle.dump(permission_list, open(PERMISSION_LIST, "wb"))

    ext = list(da.get_external_classes())
    mthd_list = list()
    for cls_ans in ext:
        vm_cls = cls_ans.get_vm_class()
        # Skip the ; in the end
        name = vm_cls.get_name()[:-1]
        methods = vm_cls.get_methods()
        for method in methods:
            mthd_name = method.get_name()
            mthd_list.append(name + CONNECTOR + mthd_name)

    for api in mthd_list:
        if api in api_list.keys():
            api_list[api] += 1
        else:
            api_list[api] = 1

    pickle.dump(api_list, open(API_LIST, "wb"))

except Exception as e:
    print(e)
    print("Skipping {}".format(filename))
