import os
import gc
import sys
import numpy as np
import pandas as pd
import sys
from api_calls import *

filename = sys.argv[1]

CORPUS_FOLDER = '../../Test Cases/Benign'
ANDROGUARD_FOLDER = '../androguard'
PERM_CSV_FILE = 'perm_data.csv'
API_CSV_FILE = 'api_data.csv'
PERMISSION_LIST = 'perm_list.txt'

# Import Androguard
sys.path.append(ANDROGUARD_FOLDER)
import androlyze as anz


# Read the permission list file
with open(PERMISSION_LIST) as f:
    permission_list = f.readlines()

permission_list = sorted(list(set([x.strip() for x in permission_list])))

# Now try to create a vector
if not os.path.isfile(PERM_CSV_FILE):
    # Since the file is not there, we need to create a blank file
    df = pd.DataFrame(index=[], columns=permission_list)
    df = df.fillna(0)
    df.to_csv(PERM_CSV_FILE)

if not os.path.isfile(API_CSV_FILE):
    # Since the file is not there, we need to create a blank file
    df = pd.DataFrame(index=[], columns=MAPPINGS)
    df = df.fillna(0)
    df.to_csv(API_CSV_FILE)

try:
    APK_FILE = filename
    print(APK_FILE)
    # data = dict()
    # Split the file into APK object, DalvikVMObject and Analysis object
    apk, dvmf, da = anz.AnalyzeAPK(os.path.join(CORPUS_FOLDER, APK_FILE))

    # Extract the Android Manifest file, and then select children
    permissions = apk.get_permissions()
    # data['permissions'] = permissions
    print("Permissions:")
    for perms in permissions:
        print(perms)
    perm_cols = [1 if x in permissions else 0 for x in permission_list]
    perm_cols = [perm_cols]
    perm_cols = np.array(perm_cols)

    # Get all external classes (i.e. API classes)
    ext = list(da.get_external_classes())

    mthd_list = list()
    for cls_ans in ext:
        vm_cls = cls_ans.get_vm_class()
        # Skip the ; in the end
        name = vm_cls.get_name()[:-1]
        methods = vm_cls.get_methods()
        for method in methods:
            mthd_name = method.get_name()
            mthd_list.append(name + CONNECTOR + mthd_name)
    print("\nAPI calls:")
    for methd in mthd_list:
        print(methd)

    final_list = [1 if x in mthd_list else 0 for x in MAPPINGS]
    api_cols = [final_list]
    api_cols = np.array(api_cols)

    df = pd.DataFrame(perm_cols, index=[filename], columns=permission_list)
    with open(PERM_CSV_FILE, 'a') as f:
        df.to_csv(f, header=False)

    df2 = pd.DataFrame(api_cols, index=[filename], columns=MAPPINGS)
    with open(API_CSV_FILE, 'a') as f:
        df2.to_csv(f, header=False)
    del apk
    del dvmf
    del da
    # gc.collect()
except Exception as e:
    print(e)
    print("Skipping {}".format(filename))
