import pandas as pd
import numpy as np
from dbn import SupervisedDBNClassification

df = pd.read_csv('perm_data.csv')
df = df.replace('M', 1)
df = df.replace('B', 0)
X1 = df.as_matrix()[:, 1:-1]
final_y = df.as_matrix()[:,(-1)]


df = pd.read_csv('api_data.csv')
df = df.replace('M', 1)
df = df.replace('B', 0)
X2 = df.as_matrix()[:, 1:-1]

final_X =np.hstack((X1, X2)).astype(np.float32)
print(final_X)

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
X_train, X_test, Y_train, Y_test = train_test_split(final_X, final_y, test_size=0.2, random_state=0)

hidden_layers = [[256], [256] * 2, [256] * 5, [256] * 4, [256] * 3, [256] * 10]

for layer in hidden_layers:
    classifier = SupervisedDBNClassification(hidden_layers_structure=layer,
                                             learning_rate_rbm=0.05,
                                             learning_rate=0.1,
                                             n_epochs_rbm=10,
                                             n_iter_backprop=100,
                                             batch_size=32,
                                             activation_function='relu',
                                             dropout_p=0.2)


    classifier.fit(X_train, Y_train)

    # Save the model
    classifier.save('models/model_hidden' + str(len(layer)) + '.pkl')

    # Restore it
    classifier = SupervisedDBNClassification.load('models/model_hidden' + str(len(layer)) + '.pkl')

    # Test
    Y_pred = classifier.predict(X_test)
    print('Done.\nAccuracy for DBN with ', len(layer) ,': %f' % accuracy_score(list(Y_test), Y_pred))
