from collections import OrderedDict

API_CALLS = {
    'Landroid/telephony/TelephonyManager': ['getSubscriberId', 'getDeviceId', 'getLine1Number'],
    'Landroid/app/Service': ['onDestroy', 'onCreate', '<init>'],
    'Landroid/content/Context': ['startService'],
    'Landroid/content/pm/PackageManager': ['getInstalledPackages'],
    'Landroid/telephony/SmsManager': ['getDefault', 'sendTextMessage'],
    'Ljava/util/Timer': ['<init>'],
    'Landroid/os/Bundle': ['get'],
    'Landroid/content/pm/ApplicationInfo': ['loadLabel'],
    'Ljava/lang/Process': ['getOutputStream', 'waitFor'],
    'Ljava/lang/Runtime': ['exec'],
    'Ljava/util/TimerTask': ['<init>'],
    'Ljava/io/DataOutputStream': ['flush'],
    'Ljava/io/FileOutputStream': ['flush'],
    'Landroid/net/NetworkInfo': ['getExtraInfo'],
}

API_CALLS = OrderedDict(sorted(API_CALLS.items(), key=lambda t: t[0]))

CONNECTOR = '->'

MAPPINGS = []

for values in API_CALLS:
    methods = API_CALLS[values]
    for method in methods:
        MAPPINGS.append(values + CONNECTOR + method)

MAPPINGS = sorted(MAPPINGS)
